# athena-support
目前功能：

1. 消息类型：文本消息、图片消息、自定义消息
2. 渠道划分及访问来源统计
3. 客服的动态分配机制
4. 上下班时间控制
5. 自动回复、快捷回复
6. 自动接待、手动接待、转接会话
7. 多端同时登录
8. 客服分组管理

> 需要改动Vue组件的话，安装gulp，并在根目录下执行便可，怎么用就不说了，按照常识走就行。
> 前端项目由vue-cli手脚架生成，稍微改动了下。发布使用"gulp"，开发使用"gulp dev"，可以在java环境中开发使用还算方便。

预计支持平台：Html5、Chrome App、iOS、Android
### 单机版Download
链接：http://pan.baidu.com/s/1o7Al2Dc 密码：ite5

安装教程：

首先是消息服务器的启动，位于athena目录
1. 创建message数据库，message.sql数据库导入库中
2. 修改config文件，配置好mysql与redis地址
3. 启动athena，默认端口号为8080和18181，更改端口可参考bat文件。
4. 启动后可在http://ip/login.html登入，默认账号密码为 ```admin@opdar.com``` ```admin```
5. 进入后可创建appId作为消息应用Id，默认带了一个。单机版目前看不到系统信息，分布式版本可以做“多服务器间的用户迁移”和“在线用户数”查看。

然后是客服系统的配置，位于support目录
1. 创建support数据库，support.sql导入库中
2. 修改config文件，数据库的设置，消息服务器的地址及协议，以及目前通过七牛实现上传图片（后期会加其他方式），需要申请一个七牛账号，填入ak\sk\domain等信息。
3. 启动客服服务，默认端口号为8081，可参考startup.bat中自行更改
4. 默认自带一个用户，账号密码为 ```admin``` ```admin```

几点使用说明：

1. 账号创建后角色为管理员，可于设置中获取AppId和新增客服的操作
2. 两个账号同时登陆的话，可以使用两个浏览器登陆，不然session会冲突，或使用SessionBox这类插件保持两个会话即可。
3. 一个账号多人登陆的话，会互踢，所以如果收不到消息了，多半是这个账号在其他地方登陆了，测试的话可以自行注册客服账号。
4. 由于本人是用Chrome作为开发浏览器，并接下来考虑会上架ChromeApp，所以使用时尽可能的使用Chrome作为第一浏览器。
5. 演示地址带宽有限（1M），消息服务器的话走vpn反向代理到家里了，前端在腾讯的CDN上，不是很稳定，有钱的大佬可以支持一下。
6. 以下是演示地址和账号密码，建议执行注册测试账号，但请不要恶意注册谢谢。

```
客服端登陆
http://support.opdar.com/login.html
账号：admin 密码：admin
客服端注册(临时)
http://support.opdar.com/register.html
客户端（H5）：
http://support.opdar.com/client/login?appId=
appId可于客服端内的“配置”选项中获取，可选择注册账户登陆或匿名登陆
自定义消息测试页面：
http://otfn8pxlr.bkt.clouddn.com/test.html
```

使用以下项目：
1. http://git.oschina.net/opdar/athena
2. http://git.oschina.net/opdar/gulosity-web-platform
3. http://git.oschina.net/opdar/gulosity-mybatis-plugin

关于里程碑：

下阶段预计会加入的功能有：

1. 客户评价
2. 同事互动
3. 微信客服接入

工单系统因为比较复杂，目前时间有限，等有雏形后会慢慢集成进来。