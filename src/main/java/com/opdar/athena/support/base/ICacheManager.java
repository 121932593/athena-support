package com.opdar.athena.support.base;

/**
 * Created by shiju on 2017/8/7.
 */
public interface ICacheManager<K,K2,V> {
    public interface Watcher{

    }
    void set(K key,V value);
    void hset(K key,K2 key2,V value);
    void hdel(K key,K2 key2);
    V get(K key);
    V hget(K key,K2 key2);
    boolean exist(K key);
    void setWatcher(K key,Watcher watcher);
}
