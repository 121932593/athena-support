package com.opdar.athena.support.base;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shiju on 2017/8/7.
 */
public class GlobalCacheManager implements ICacheManager<String,String,Object>{
    private ConcurrentHashMap<String,byte[]> store = new ConcurrentHashMap<String,byte[]>();
    private ConcurrentHashMap<String,ConcurrentHashMap<String,byte[]>> hashStore = new ConcurrentHashMap<String,ConcurrentHashMap<String,byte[]>>();

    @Override
    public void set(String key, Object value) {
        put(key, value, store);
    }

    @Override
    public void hset(String key, String key2, Object value) {
        ConcurrentHashMap<String, byte[]> hash;
        if(hashStore.containsKey(key)){
            hash = hashStore.get(key);
        }else{
            hash = new ConcurrentHashMap<String, byte[]>();
            hashStore.put(key,hash);
        }

        put(key2, value, hash);
    }

    @Override
    public void hdel(String key, String key2) {
        if(hashStore.containsKey(key)){
            ConcurrentHashMap<String, byte[]> hash = hashStore.get(key);
            if(hash.containsKey(key2))hash.remove(key2);
        }
    }

    private void put(String key, Object value, ConcurrentHashMap<String, byte[]> hash) {
        ObjectOutputStream oos = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(value);
            byte[] bytes = baos.toByteArray();
            hash.put(key,bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(oos != null) try {
                oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object get(String key) {
        try {
            return getValue(key, store);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object hget(String key, String key2) {
        if(!hashStore.containsKey(key))return null;
        ConcurrentHashMap<String, byte[]> hash = hashStore.get(key);
        try {
            return getValue(key2,hash);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object getValue(String key, ConcurrentHashMap<String, byte[]> hash) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = null;
        try{
            byte[] bytes = hash.get(key);
            if(bytes == null)return null;
            InputStream inputStream = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(inputStream);
            return ois.readObject();
        }finally {
            if(ois!=null)ois.close();
        }
    }

    @Override
    public boolean exist(String key) {
        return store.containsKey(key);
    }

    @Override
    public void setWatcher(String key, Watcher watcher) {

    }
}
