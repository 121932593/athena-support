package com.opdar.athena.support.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.opdar.athena.support.base.Constants;
import com.opdar.platform.utils.MD5;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by shiju on 2017/7/11.
 */
@Component
public class MessageUtils {
    @Value("${athena.web.address}")
    private String BASE_URL;
    @Value("${athena.appId}")
    private String APPID;
    @Value("${athena.appSecret}")
    private String APPSECRET;
    private String MESSAGE_SEND = "message/send";
    private String MESSAGE_NOTIFY = "message/notify";
    private String REGISTER = "register";
    private String LOGIN = "login";
    private String CHECK = "check";

    public JSONObject login(String userName, String userPwd, Integer platform){
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("userName",userName);
        parameters.put("userPwd",userPwd);
        parameters.put("platform",platform);
        getParameter(parameters);
        try {
            String result = NetTool.post(BASE_URL+LOGIN,parameters);
            JSONObject object = JSON.parseObject(result);
            if(!object.containsKey("code")){
                return object;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer check(String token){
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("token",token);
        getParameter(parameters);
        try {
            String result = NetTool.post(BASE_URL+CHECK,parameters);
            JSONObject object = JSON.parseObject(result);
            return object.getInteger("code");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject regist(String userName, String userPwd){
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("userName",userName);
        parameters.put("userPwd",userPwd);
        getParameter(parameters);
        try {
            String result = NetTool.post(BASE_URL+REGISTER,parameters);
            JSONObject object = JSON.parseObject(result);
            if(!object.containsKey("code")){
                return object;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getParameter(Map<String,Object> map){
        map.put("appId", APPID);
        map.put("timestamp",System.currentTimeMillis());
        TreeMap<String,Object> parameters = new TreeMap<String, Object>();
        parameters.putAll(map);
        String sign = getSign(parameters);
        map.put("signature",sign);
    }

    private String getSign(TreeMap<String, Object> parameters) {
        StringBuilder builder = new StringBuilder();
        for (String key : parameters.keySet()) {
            builder.append(key).append("=").append(parameters.get(key));
        }
        builder.append(APPSECRET);
        return MD5.encrypt(builder.toString());
    }

    public JSONObject notify(String messageId, String token) {
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("messageId",messageId);
        parameters.put("token",token);
        getParameter(parameters);
        try {
            String result = NetTool.post(BASE_URL+MESSAGE_NOTIFY,parameters);
            JSONObject object = JSON.parseObject(result);
            if(!object.containsKey("code")){
                return object;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject send(String content,Integer type,String sender,String reciver,String fakeId,String token) {
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("content",content);
        parameters.put("type",type);
        parameters.put("reciver",reciver);
        parameters.put("sender",sender);

        parameters.put("fakeId",fakeId);
        parameters.put("token",token);
        getParameter(parameters);
        try {
            String result = NetTool.post(BASE_URL+MESSAGE_SEND,parameters);
            JSONObject object = JSON.parseObject(result);
            if(!object.containsKey("code")){
                return object;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
