package com.opdar.athena.support.controllers.support;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Constants;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.AppConfigEntity;
import com.opdar.athena.support.entities.SupportConfigEntity;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.mapper.AppConfigMapper;
import com.opdar.athena.support.mapper.SupportConfigMapper;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
@Interceptor(CommonInterceptor.class)
public class SettingController {

    @Autowired
    SupportConfigMapper supportConfigMapper;
    @Autowired
    AppConfigMapper appConfigMapper;

    @Request(value = "/setting/base/quickreply", format = Request.Format.JSON)
    public Result supportQuickReplyGet() {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        SupportConfigEntity config = new SupportConfigEntity();
        config.setName(Constants.QUICK_REPLY);
        config.setSupportId(user.getId());
        config = supportConfigMapper.selectOne(config);
        if (config != null && config.getStat() == 1 && !StringUtils.isEmpty(config.getConfig())) {
            JSONArray reply = JSON.parseArray(config.getConfig());
            return Result.valueOf(reply);
        }
        return Result.valueOf(new ArrayList());
    }

    @Request(value = "/setting/base/update", format = Request.Format.JSON)
    public Object settingBaseUpdate(SupportConfigEntity supportConfigEntity) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        SupportConfigEntity where = new SupportConfigEntity();
        where.setName(supportConfigEntity.getName());
        where.setSupportId(user.getId());

        SupportConfigEntity update = new SupportConfigEntity();
        update.setStat(supportConfigEntity.getStat());
        if (!StringUtils.isEmpty(supportConfigEntity.getConfig())) {
            update.setConfig(supportConfigEntity.getConfig());
        }
        update.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        if (supportConfigMapper.count(where) > 0) {
            supportConfigMapper.update(update, where);
        } else {
            update.setName(where.getName());
            update.setSupportId(where.getSupportId());
            update.setCreateTime(new Timestamp(System.currentTimeMillis()));
            supportConfigMapper.insert(update);
        }
        return Result.valueOf(null);
    }

    @Request(value = "/setting/base/find", format = Request.Format.JSON)
    public Object settingBaseFind(SupportConfigEntity supportConfigEntity) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");

        SupportConfigEntity where = new SupportConfigEntity();
        where.setSupportId(user.getId());
        List<SupportConfigEntity> list = supportConfigMapper.selectList(where);
        Map<String, Object> map = new HashMap<String, Object>();
        for (SupportConfigEntity supportConfigEntity1 : list) {
            Map<Object, Object> result = new HashMap<Object, Object>();
            result.put("stat", supportConfigEntity1.getStat());
            try {
                result.put("config", JSON.parse(supportConfigEntity1.getConfig()));
            } catch (Exception e) {
                result.put("config", supportConfigEntity1.getConfig());
            }

            map.put(supportConfigEntity1.getName(), result);
        }
        return Result.valueOf(map);
    }

    @Request(value = "/setting/advance/find", format = Request.Format.JSON)
    public Result settingAdvanceFind(String key) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        AppConfigEntity appConfigEntity = new AppConfigEntity();
        appConfigEntity.setAppId(user.getAppId());
        appConfigEntity.setKey(key);
        appConfigEntity = appConfigMapper.selectOne(appConfigEntity);
        return Result.valueOf(appConfigEntity);
    }

    @Request(value = "/setting/advance/update", format = Request.Format.JSON)
    public Result settingAdvanceUpdate(String key,String value) {
        if(!StringUtils.isEmpty(key) && !StringUtils.isEmpty(value)){
            SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
            AppConfigEntity appConfigEntity = new AppConfigEntity();
            appConfigEntity.setAppId(user.getAppId());
            appConfigEntity.setKey(key);
            appConfigEntity = appConfigMapper.selectOne(appConfigEntity);
            if(appConfigEntity == null){
                appConfigEntity = new AppConfigEntity();
                appConfigEntity.setId(UUID.randomUUID().toString());
                appConfigEntity.setAppId(user.getAppId());
                appConfigEntity.setKey(key);
                appConfigEntity.setValue(value);
                appConfigEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
                appConfigEntity.setUpdateTime(new Timestamp(System.currentTimeMillis()));
                appConfigMapper.insert(appConfigEntity);
            }else {
                AppConfigEntity update = new AppConfigEntity();
                AppConfigEntity where = new AppConfigEntity();
                where.setAppId(user.getAppId());
                where.setKey(key);
                update.setValue(value);
                update.setUpdateTime(new Timestamp(System.currentTimeMillis()));
                appConfigMapper.update(update,where);
            }
            return Result.valueOf(appConfigEntity);
        }
        return Result.valueOf(1,"更新失败");
    }
}
