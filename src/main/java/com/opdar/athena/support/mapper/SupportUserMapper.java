package com.opdar.athena.support.mapper;

import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.plugins.mybatis.core.IBaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by shiju on 2017/7/11.
 */
public interface SupportUserMapper extends IBaseMapper<SupportUserEntity> {
    List<SupportUserEntity> selectByGroup(@Param("groupId") String groupId, @Param("appId") String appId);
}
